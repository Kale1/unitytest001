﻿using UnityEngine;

public class AstroidHandler : MonoBehaviour 
{
	public float speed;

	public  bool    left;
	private Vector3 leftSide;
	private Vector3 rightSide;
	void Start()
	{
		float padding = 1.5f;

		leftSide  = Camera.main.transform.position;
		rightSide = Camera.main.transform.position;
		leftSide.x  = MainCameraTools.cameraLeft  + padding;
		rightSide.x = MainCameraTools.cameraRight - padding;

		leftSide.z  = transform.position.z;
		rightSide.z = transform.position.z;
	}

	void Update()
	{
		if (left && transform.position.x > leftSide.x)
		{
			transform.position = Vector3.Lerp(transform.position, leftSide, speed * Time.deltaTime);
		}
		if (!left && transform.position.x < rightSide.x)
		{
			transform.position = Vector3.Lerp(transform.position, rightSide, speed * Time.deltaTime);
		}
	}	

	void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log("hit");

		other.gameObject.GetComponent<SpaceshipHandler>().Die();
	}
	public void ToggleSide()
	{
		left = !left;
	}


}
