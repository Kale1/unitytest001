﻿using UnityEngine;

public class MainCameraTools
{	
	public static readonly Vector3 position      = Camera.main.transform.position;
	public static readonly float   halfWidth     = Camera.main.orthographicSize * Camera.main.aspect;
	public static readonly float   heightInUnits = Camera.main.orthographicSize * 2;
	public static readonly float   cameraTop     = Camera.main.transform.position.y + Camera.main.orthographicSize;
	public static readonly float   cameraBottom  = Camera.main.transform.position.y - Camera.main.orthographicSize;
	public static readonly float   cameraLeft    = Camera.main.transform.position.x - halfWidth;
	public static readonly float   cameraRight   = Camera.main.transform.position.x + halfWidth;

}
