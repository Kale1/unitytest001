﻿using UnityEngine;

public class ConstantMotion : MonoBehaviour
{
	public Vector2 velocity;

	private Rigidbody2D body;
	void Start()
	{
		body = GetComponent<Rigidbody2D>();
		body.velocity = velocity;
	}

	void Stop()
	{
		body.velocity = Vector2.zero;
	}
}
