﻿using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
	public  Vector3 rotationSpeeds;
	private Vector3 newRotation;

	void Start()
	{
		newRotation = transform.rotation.eulerAngles;	
	}

	void Update()
	{
		newRotation += rotationSpeeds * Time.deltaTime;
		transform.rotation = Quaternion.Euler(newRotation);
	}
}
