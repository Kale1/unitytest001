﻿using UnityEngine;

public class SpaceshipHandler : MonoBehaviour
{
	public float padding;

	private Vector3 cameraBottom;
	private Vector3 cameraTop;
	
	private Animator animator;
	private AudioSource sound;

	private bool isDead;
	void Start () 
	{
		isDead = false;

		animator = GetComponent<Animator>();
		sound    = GetComponent<AudioSource>();
		
		cameraBottom   = cameraTop = MainCameraTools.position;
		cameraBottom.y = MainCameraTools.cameraBottom - padding;	
		cameraBottom.z = transform.position.z;
		cameraTop.y    = MainCameraTools.cameraTop    + padding;	
		cameraTop.z    = transform.position.z;
	}

	void Update()
	{
		if (transform.position.y < cameraBottom.y)
		{
			transform.position = cameraTop;
		}
	}

	public void Die()
	{
		if(isDead) 
		{
			return;
		}
		
		SendMessage("Stop");
		animator.SetBool("isDead", true);
		sound.Play();
		isDead = true;
		Destroy(gameObject, 1.25f);
	}
}
