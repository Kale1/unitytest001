﻿using UnityEngine;

//Spawns as many ships as can fit on the camera
public class SpaceshipSpawner : MonoBehaviour 
{	
	public GameObject ship;
	public float      padding;
	void OnEnable () 
	{
		float spriteHeight = ship.GetComponent<SpriteRenderer>().sprite.bounds.size.y + padding;
		int   totalShips   = Mathf.FloorToInt(MainCameraTools.heightInUnits / spriteHeight);	
		
		Vector2 startPos = Camera.main.transform.position;
		startPos.x  = transform.position.x;
		startPos.y += Camera.main.orthographicSize - spriteHeight;

		GameObject spawnedShip;
		for (int i = 0; i < totalShips; ++i)
		{
			spawnedShip = Instantiate(ship, startPos, Quaternion.identity);
			spawnedShip.transform.parent = gameObject.transform;
			startPos.y -= spriteHeight;
			if (startPos.y < Camera.main.transform.position.y - Camera.main.orthographicSize + spriteHeight)
			{
				break;
			}
		}
	}
}
