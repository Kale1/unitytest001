﻿using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour 
{

	public Text       shipsLeft;
	public GameObject spawner;

	private int previousCount;
	void Start()
	{
		previousCount = -1;
	}
	void Update()
	{
		if (previousCount != spawner.transform.childCount)
		{
			shipsLeft.text = "Ships Remaining: " + spawner.transform.childCount;
			previousCount  = spawner.transform.childCount;
		}
	}

	public void LoadMoreShips()
	{
		if (spawner.transform.childCount != 0) 
		{
			return;
		}
		else
		{
			spawner.SetActive(false);
			spawner.SetActive(true);
		}
	}
}
